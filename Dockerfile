FROM python:3.7

WORKDIR /code    
COPY . ./
RUN chmod +x init.sh start.sh
RUN pip install pipenv
RUN pipenv install --deploy --system

